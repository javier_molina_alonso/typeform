import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by javier on 04/08/15.
 */
public class Mamon {

    private static final String ENTRYPOINT = "http://aerial-valor-93012.appspot.com/challenge";
    private static final String SEPARATOR = "/";

    public static void main(String[] args) throws Exception {
        Challenge challenge = requestChallenge();
        String challengeSolution = solveChallenge(challenge);
        System.out.println(challengeSolution);
    }

    private static Challenge requestChallenge() throws IOException {
        URL url = new URL(ENTRYPOINT);
        String response = parseOneLineResponse(url);
        return createChallenge(response);
    }

    private static String solveChallenge(Challenge challenge) throws IOException {
        Integer result = 0;
        if(challenge.values != null && challenge.values.length > 0) {
            for (String currentValue : challenge.values) {
                result += Integer.valueOf(currentValue);
            }
        }
        URL url = new URL(ENTRYPOINT + SEPARATOR + challenge.token + SEPARATOR + result.toString());
        return parseOneLineResponse(url);
    }

    private static String parseOneLineResponse(URL url) throws IOException {
        URLConnection c = url.openConnection();
        c.setDoInput(true);
        InputStream inStream = c.getInputStream();
        BufferedReader input = new BufferedReader(new InputStreamReader(inStream));
        return input.readLine();
    }

    private static Challenge createChallenge(String line) {
        return new Gson().fromJson(line, Challenge.class);
    }

    private static class Challenge {
        private final String token;
        private final String[] values;

        public Challenge(String token, String[] values) {
            this.token = token;
            this.values = values;
        }
    }
}
